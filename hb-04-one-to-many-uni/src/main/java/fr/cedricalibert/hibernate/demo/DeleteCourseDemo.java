package fr.cedricalibert.hibernate.demo;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import fr.cedricalibert.hibernate.demo.entity.Course;
import fr.cedricalibert.hibernate.demo.entity.Instructor;
import fr.cedricalibert.hibernate.demo.entity.InstructorDetail;

public class DeleteCourseDemo {

	public static void main(String[] args) {
		//create session factory 
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Instructor.class)
								.addAnnotatedClass(InstructorDetail.class)
								.addAnnotatedClass(Course.class)
								.buildSessionFactory();
		
		//create session 
		Session session = factory.getCurrentSession();
		
		try {
			
			//start a transaction
			session.beginTransaction();
			
			//get the course
			int id = 10;
			Course course = session.get(Course.class, id);
			
			System.out.println("Course " + course);
			
			//delete the instructor course
			System.out.println("Deleting instructor course");
			
			session.delete(course);
			
			//commit transaction
			session.getTransaction().commit();
		
			
			System.out.println("Done");
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			session.close();
			factory.close();
		}

	}

}
