package fr.cedricalibert.hibernate.demo;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import fr.cedricalibert.hibernate.demo.entity.Course;
import fr.cedricalibert.hibernate.demo.entity.Instructor;
import fr.cedricalibert.hibernate.demo.entity.InstructorDetail;

public class CreateInstructorDemo {

	public static void main(String[] args) {
		//create session factory 
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Instructor.class)
								.addAnnotatedClass(InstructorDetail.class)
								.addAnnotatedClass(Course.class)
								.buildSessionFactory();
		
		//create session 
		Session session = factory.getCurrentSession();
		
		try {
			
			//create the objects
			
			Instructor instructor = new Instructor("Cédric", "ALIBERT", "test@test.fr");
			InstructorDetail instructorDetail = new InstructorDetail("Test", "Code");
			
			//associate the objetcs together
			instructor.setInstructorDetail(instructorDetail);
			
			//start a transaction
			session.beginTransaction();
			
			//save the instructor
			//because cascade type, save also the instructor detail
			System.out.println("Saving instructor : "+instructor);
			session.save(instructor);
			
			//commit transaction
			session.getTransaction().commit();
		
			
			System.out.println("Done");
			
		}
		finally {
			session.close();
			factory.close();
		}

	}

}
